# SMRT S&C Learning Git Repository
### To-Dos

- Requisition Projector
  - Done
- Address Comments on Slides
  - Done
- Prepare Exercises
  - Done
- Prepare Cheat Sheets
  - Done
- Send Meeting Reminder
  - No Need La

### Resources
- Udacity�s Version Control with Git
  - https://www.udacity.com/course/version-control-with-git--ud123
- Interactive Git Branching
  - https://learngitbranching.js.org/
- Pro Git
  - https://git-scm.com/book/en/v2
  - Distributed Git: Contributing to a Project 
  - Distributed Git: Maintaining a Project
